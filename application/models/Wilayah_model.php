<?php
class Wilayah_model extends CI_Model {
	
	private $tabel	= "wilayah";
	
	public function data()
	{
		$query 		= $this->db->get($this->tabel);
		return $query->result();
	}

	public function tambah($post){
		$post->dibuat	= date("Y-m-d H:i:s");
		return $this->db->insert($this->tabel, $post);
	}

	public function ubah($id, $post){
		$post->diubah	= date("Y-m-d H:i:s");
		$this->db->where("id", $id);
		return $this->db->update($this->tabel, $post);
	}

	public function hapus($id){
		$this->db->where("id", $id);
		return $this->db->delete($this->tabel);
	}
}