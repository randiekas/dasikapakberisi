<?php
class Akun_model extends CI_Model {

	private $tabel	= "akun";

	public function ada_by_username_password($username, $sandi)
	{
		$this->db->select("id");
		$this->db->where("username", $username);
		$this->db->where("sandi", md5($sandi));
		$this->db->where("aktif", 1);
		$query 		= $this->db->get($this->tabel);
		return $query->result();
	}

	public function ada_by_username($username, $kolom="id")
	{
		$this->db->select($kolom);
		$this->db->where("username", $username);
		$query 		= $this->db->get($this->tabel);
		return $query->result();
	}

	public function detil($id, $kolom="*"){
		$this->db->select($kolom);
		$this->db->where("id", $id);
		$query		= $this->db->get($this->tabel);
		return $query->last_row();
	}

	public function ubah_terakhir_masuk($id){
		$this->db->where("id", $id);
		$data["terakhir_masuk"]	= date("Y-m-d H:i:s");
		$this->db->update($this->tabel, $data);
	}
	
	public function ubah($id, $post){
		$post->diubah	= date("Y-m-d H:i:s");
		$this->db->where("id", $id);
		return $this->db->update($this->tabel, $post);
	}

	public function tambah($post){
		$post['aktif']	= 0;
		$post['dibuat']	= date("Y-m-d H:i:s");
		$this->db->insert($this->tabel, $post);
		return $this->db->insert_id();
	}

	public function hapus($id){
		$this->db->where("id", $id);
		return $this->db->delete($this->tabel);
	}

}