<?php
class Pegawai_model extends CI_Model {

	private $tabel	= "pegawai";

	public function data()
	{
		$query 		= $this->db->get($this->tabel);
		return $query->result();
	}
	
	public function detilByUserID($userId, $kolom="*")
	{	
		$this->db->select($kolom);
		$this->db->where("user_id", $userId);
		$query 		= $this->db->get($this->tabel);
		return $query->last_row();
	}

	public function detil($id, $kolom="*")
	{	
		$this->db->select($kolom);
		$this->db->where("id", $id);
		$query 		= $this->db->get($this->tabel);
		return $query->last_row();
	}

	public function tambah($post){
		$post->dibuat	= date("Y-m-d H:i:s");
		return $this->db->insert($this->tabel, $post);
	}

	public function ubah($id, $post, $id_kolom="id"){
		$post->diubah	= date("Y-m-d H:i:s");
		$this->db->where($id_kolom, $id);
		return $this->db->update($this->tabel, $post);
	}

	public function hapus($id){
		$this->db->where("id", $id);
		return $this->db->delete($this->tabel);
	}

	public function dataTim($tipe, $wilayah=null){
		if($tipe=="manager"){
			$this->db->select("tim_manager");
			$this->db->group_by("tim_manager");
			$this->db->where('tim_manager is NOT NULL', NULL, FALSE);
		}else{
			$this->db->select("tim_head");
			$this->db->group_by("tim_head");
			$this->db->where('tim_head is NOT NULL', NULL, FALSE);
		}
		if($wilayah!=null){
			$this->db->where("wilayah", $wilayah);
		}
		
		return $this->db->get($this->tabel)->result();
	}
}