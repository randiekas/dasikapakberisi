<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// $this->load->model("akun_model", "model");
	}
	
	public function statistik(){

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= [];
		$response["data"]["target_poin_tim_bulan"]	= 0;
		$response["data"]["pencapaian_tim_bulan"]	= 0;
		$response["data"]["sisa_poin_tim_bulan"]	= 0;
		$response["data"]["cso"]					= "-";
		$response["data"]["telepon_cso"]			= "-";
		$response["data"]["telepon_cso"]			= "-";
		json($response);
	}

	public function pencapaianTeam(){

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= [];
		$response["data"]["target_poin_tim_bulan"]	= 0;
		$response["data"]["pencapaian_tim_bulan"]	= 0;
		$response["data"]["sisa_poin_tim_bulan"]	= 0;
		$response["data"]["sisa_hari_bulan"]		= 1;
		$response["data"]["list"]= [];
		json($response);
	}
}
