<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("akun_model", "model");
	}
	public function masuk(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= "";

		// kondisi masuk
		$cek						= $this->model->ada_by_username_password($post->username, $post->sandi);
		if(count($cek)==0){
			$response["status"]		= false;
			$response["message"]	= "Username dan password salah !";
		}else{
			$akun					=  $this->model->detil($cek[0]->id);
			$jwt["id"]				= $akun->id;
			$jwt["tipe"]			= $akun->tipe;
			$token					= jwt::encode($jwt, $this->config->item("jwt_key"));

			$data					= $akun;
			$data->token			= $token;
			$response["data"]		= $data;

			$this->model->ubah_terakhir_masuk($akun->id);
		}
		json($response);
	}

	public function dataByUsername(){
		// setdata
		$data			= [];
		$post 			= $this->input->get("username");

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= "";

		// kondisi masuk
		$cek						= $this->model->ada_by_username($post);
		if(count($cek)==0){
			$response["status"]		= false;
			$response["message"]	= "akun belum ada";
		}else{
			$this->load->model("pegawai_model");
			$akun					=  $this->model->detil($cek[0]->id);
			$data					= $akun;

			$jwt["id_akun"]			= $akun->id;
			$jwt["tipe"]			= "publik";
			$token					= jwt::encode($jwt, $this->config->item("jwt_key"));
			$data->token			= $token;
			$data->biodata			= $this->pegawai_model->detilByUserId($post);
			$response["data"]		= $data;
		}
		
		json($response);
	}

	public function setAktif(){
		$this->load->model("pegawai_model");
		// setdata
		cek_akses();
		$data			= [];
		$post 			= post_data();
		
		// set reponse
		$id						= jwt()->id_akun;
		$akun					= $this->model->detil($id);
		$post->sandi			= md5($post->sandi);
		$post->aktif			= 1;
		$post_pegawai			= [];

		$post_akun				= [];

		$response["status"]		= true;
		if($akun->tipe=="manager"){
			$this->db->select("id");
			$this->db->where("tim_manager", $post->tim_manager);
			$cek	= $this->db->get("pegawai")->result();
			if(count($cek)==0){
				$post->tim_manager		= $post->tim_manager;
			}else{
				$response["status"]		= false;
				$response["message"]	= "nama tim manager sudah dipakai, silahkan gunakan nama tim lain";
			}
		}else if($akun->tipe=="head"){

			$this->db->select("id");
			$this->db->where("tim_manager", $post->tim_manager);
			$cek	= $this->db->get("pegawai")->result();
			if(count($cek)==0){
				$response["status"]		= false;
				$response["message"]	= "nama tim manager tidak ditemukan";
			}else{
				$this->db->select("id");
				$this->db->where("tim_head", $post->tim_head);
				$cek	= $this->db->get("pegawai")->result();
				if(count($cek)==0){
					$post->tim_head			= $post->tim_head;
				}else{
					$response["status"]		= false;
					$response["message"]	= "nama tim head sudah dipakai, silahkan gunakan nama tim lain";
				}
			}
		}else if($akun->tipe=="sales"){
			$this->db->select("id");
			$this->db->where("tim_head", $post->tim_head);
			$cek	= $this->db->get("pegawai")->result();
			if(count($cek)==0){
				$response["status"]		= false;
				$response["message"]	= "nama tim head tidak ditemkan";
			}
		}
		if($response["status"]){
			$post_akun["sandi"]		= $post->sandi;
			$post_akun["aktif"]		= $post->aktif;
			$post_akun				= (object)$post_akun;
			$response["status"]		= $this->model->ubah($id, $post_akun);

			$post_pegawai["tim_manager"]= @$post->tim_manager;
			$post_pegawai["tim_head"]	= @$post->tim_head;
			$post_pegawai				= (object)$post_pegawai;
			$this->pegawai_model->ubah($id, $post_pegawai, "id_akun");
			
			$response["message"]	= "data berhasil diubah";
		}
		json($response);
	}
	public function tim($tipe){
		$wilayah				= $this->input->get("wilayah");
		$this->load->model("pegawai_model");
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= $this->pegawai_model->dataTim($tipe, $wilayah);

		json($response);
	}
}
