<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("wilayah_model", "model");
		
	}
	public function data(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= $this->model->data();

		json($response);
	}
	
}
