<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("pegawai_model", "model");
		$this->load->model("akun_model");
		cek_akses();
	}

	public function data(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= $this->model->data();

		json($response);
	}

	public function tambah(){
		// setdata
		$data			= [];
		$post 			= post_data();
		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";

		// cek akun,
		$cek			= $this->akun_model->ada_by_username($post->user_id);
		if(count($cek)==0){
			$post_akun["username"]	= $post->user_id;
			$post_akun["sandi"]		= $post->user_id;
			$post_akun["tipe"]		= $post->jabatan;
			$id_akun				= $this->akun_model->tambah($post_akun);
			$post->id_akun			= $id_akun;
			$response["status"]		= $this->model->tambah($post);
			$response["message"]	= "Data berhasil ditambahkan";
			
		}else{
			$response["status"]		= false;
			$response["message"]	= "Akun ini sudah digunakan";
		}
		

		json($response);
	}

	public function ubah($id){
		// setdata
		$data			= [];
		$post 			= post_data();
		$pegawai		= $this->model->detil($id, 'id_akun');
		$jwt["id"]		= $pegawai->id_akun;
		$response["data"]	= jwt::encode($jwt, $this->config->item("jwt_key"));

		// set reponse
		$response["status"]		= $this->model->ubah($id, $post);
		$response["message"]	= "data berhasil diubah";

		json($response);
	}

	public function hapus($id){
		// setdata
		$data			= [];

		$pegawai		= $this->model->detil($id, 'id_akun');

		
		// set reponse
		$response["status"]		= $this->model->hapus($id);
		$response["message"]	= "data berhasil dihapus";

		if($response["status"]){
			$this->akun_model->hapus($pegawai->id_akun);
		}

		json($response);
	}
}
