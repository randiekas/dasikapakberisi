<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("target_model", "model");
		cek_akses();
	}

	public function data(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= $this->model->data();

		json($response);
	}
	
	public function tambah(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= $this->model->tambah($post);
		$response["message"]	= "data berhasil ditambahkan";

		json($response);
	}

	public function ubah($id){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= $this->model->ubah($id, $post);
		$response["message"]	= "data berhasil diubah";

		json($response);
	}

	public function hapus($id){
		// setdata
		$data			= [];

		// set reponse
		$response["status"]		= $this->model->hapus($id);
		$response["message"]	= "data berhasil dihapus";

		json($response);
	}
}
