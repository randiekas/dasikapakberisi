<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("akun_model", "model");
	}
	
	public function masuk(){
		// setdata
		$data			= [];
		$post 			= post_data();

		// set reponse
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= "";

		// kondisi masuk
		$cek						= $this->model->ada_by_email_password($post->username, $post->sandi);
		if(count($cek)==0){
			$response["status"]		= false;
			$response["message"]	= "Username dan password salah !";
		}else{
			$akun					=  $this->model->detil($cek[0]->id);
			$jwt["id_akun"]			= $akun->id;
			$jwt["tipe"]			= $akun->tipe;
			$token					= jwt::encode($jwt, $this->config->item("jwt_key"));

			$data					= $akun;
			$data->token			= $token;
			$response["data"]		= $data;
		}
		json($response);
	}
}
